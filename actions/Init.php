<?php

namespace actions;


use base\ActionInterface;


/**
 * Инициализация приложения
 * Class Index
 * @package actions
 */
class Init implements ActionInterface
{
    public function run()
    {
        $user = new \models\UserPurse();

        $coins = [1 => 10, 2 => 30, 5 => 20, 10 => 15];
        foreach($coins as $coin => $quantity) {
            for($i=0; $i<$quantity; $i++) {
                $user->plusCoin($coin);
            }
        }
        $user->save();

        $auto = new \models\AutoPurse();
        $coins = [1 => 100, 2 => 100, 5 => 100, 10 => 100];
        foreach($coins as $coin => $quantity) {
            for($i=0; $i<$quantity; $i++) {
                $auto->plusCoin($coin);
            }
        }
        $auto->buyUserSum($auto->getUserSum());
        $auto->save();



        $productsModel = new \models\Products();

        $products = [
            ['Чай', 13, 10],
            ['Кофе', 18, 20],
            ['Кофе с молоком', 21, 20],
            ['Сок', 35, 15],
        ];

        foreach ($products as $product) {
            $productsModel->addProduct(...$product);
        }

        $productsModel->save();
    }
}