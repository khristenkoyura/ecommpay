<?php

namespace actions;


use base\ActionInterface;
use models\AutoPurse;
use models\Products;
use models\PurseInterface;
use models\UserPurse;

/**
 * Главная страница.Html шаблон
 * Class Index
 * @package actions
 */
class Index implements ActionInterface
{
    public function run()
    {
        echo file_get_contents(__DIR__ . '/../view/index.html');
    }
}