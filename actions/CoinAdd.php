<?php

namespace actions;


use base\ActionInterface;
use models\AutoPurse;
use models\PurseInterface;
use models\UserPurse;

class CoinAdd implements ActionInterface
{
    public function run()
    {
        //Валидация данных
        if (!in_array($_POST['coin'] ?? null, PurseInterface::COINS)) {
            throw new \InvalidArgumentException('Не верный формат входных данных');
        }

        $user = UserPurse::find();
        $auto = AutoPurse::find();

        $user->shiftCoin($auto, $_POST['coin']);

        $user->save();
        $auto->save();
    }
}