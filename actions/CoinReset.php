<?php

namespace actions;


use base\ActionInterface;
use models\AutoPurse;
use models\PurseInterface;
use models\UserPurse;

class CoinReset implements ActionInterface
{
    public function run()
    {
        $user = UserPurse::find();
        $auto = AutoPurse::find();

        $auto->resetUserSum($user);

        $user->save();
        $auto->save();
    }
}