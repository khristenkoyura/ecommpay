<?php

namespace actions;


use base\ActionInterface;
use models\AutoPurse;
use models\Products;


class ProductBuy implements ActionInterface
{
    public function run()
    {
        $products = Products::find();
        $auto = AutoPurse::find();

        //Валидация данных
        if (!in_array($_POST['product_id'] ?? null, $products->getProductIds())) {
            throw new \InvalidArgumentException('Товар не найден');
        }

        $products->buyAutoPurse($_POST['product_id'], $auto);

        $products->save();
        $auto->save();
    }
}