<?php

namespace actions;


use base\ActionInterface;
use models\AutoPurse;
use models\Products;
use models\PurseInterface;
use models\UserPurse;

class State implements ActionInterface
{
    public function run()
    {
        $products = Products::find();
        $auto = AutoPurse::find();
        $user = UserPurse::find();

        $data = [
            'products' => $products->getProducts(),
            'auto' => [
                'coins' => $auto->getCoins(),
                'userSum' => $auto->getUserSum(),
            ],
            'user' => [
                'coins' => $user->getCoins(),
            ],
            'coins' => PurseInterface::COINS,
        ];

        header('Content-Type: application/json');
        echo json_encode($data);
    }
}