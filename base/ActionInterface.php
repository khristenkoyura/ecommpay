<?php


namespace base;

/**
 * Итерфейс действия
 * Interface ActionInterface
 * @package base
 */

interface ActionInterface
{
    public function run();
}
