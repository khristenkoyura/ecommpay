<?php

namespace base;
use actions\Index;


/**
 * Роутер к экшенам
 * Class Router
 * @package base
 */
class Router
{
    protected $routes;

    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    /**
     * Запускаем Экшен
     */
    public function run()
    {
        $route = $this->routes[$_SERVER['REQUEST_URI']] ?? Index::class;
        /**
         * @var $action ActionInterface
         */
        $action = new $route;
        try {
            $action->run();
        }catch (\Exception $e) {
            http_response_code(422);

            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
                header('Content-Type: application/json');
                echo json_encode([
                    'error' => $e->getMessage(),
                    'code' => $e->getCode(),
                ]);
            } else {
                echo $e->getMessage();
            }
        };
    }
}