import App from './App.vue';
import Vue from 'vue'


const appClass = Vue.extend(App);
const app = new appClass();
app.$mount();
document.getElementById('app').appendChild(app.$el);