var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')


var jsFileName = '[name].js';
var cssFileName = '[name].css';
var contentBase = __dirname + '/../web/';


var devServer = {
    contentBase: contentBase,
    historyApiFallback: true,
    noInfo: true,
    port: 3000
};

devServer.host = "dev.ecommpay.tk";

var webpackConfig = {
    node: {
        net: 'empty',
        tls: 'empty',
        dns: 'empty'
    },
    entry: {
        index: './src/index.js',
    },
    output: {
        path: contentBase,
        publicPath: '/',
        filename: jsFileName
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true
                }
            }

        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: cssFileName
        }),
        new VueLoaderPlugin(),
    ],
    devServer: devServer,
    performance: {
        hints: false
    },
    devtool: '#eval-source-map'
};

if (process.env.NODE_ENV === 'production') {
    webpackConfig.devtool = '#source-map';
    webpackConfig.plugins = (webpackConfig.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ]);
}

module.exports = webpackConfig;
