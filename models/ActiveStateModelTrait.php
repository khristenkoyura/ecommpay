<?php

namespace models;


/**
 * Траит для сохранения состояни модели
 * Trait ActiveModelTrait
 * @package models
 */

trait ActiveStateModelTrait
{
    /**
     * Создаём инстанс с сохранёными данными
     * @return static
     */
    public static function find()
    {
        $model = new static;

        $path = sys_get_temp_dir() . '/' . crc32(static::class) . '.model2';
        $attributes = is_file($path) ? unserialize(file_get_contents($path)) : [];

        foreach($model->getAttributeNames() as $attribute) {
            if (isset($attributes[$attribute])) {
                $model->$attribute = $attributes[$attribute];
            }
        }

        return $model;
    }

    /**
     * Список свойств для сохранения
     * @return mixed
     */
    public function getAttributeNames()
    {
        return array_keys(get_class_vars(static::class));
    }

    /**
     * Получить список атрибутов модели
     * @return array
     */
    public function getAttributes()
    {
        $attributes = [];
        foreach($this->getAttributeNames() as $attribute) {
            $attributes[$attribute] = $this->$attribute;
        }

        return $attributes;
    }


    /**
     * Сохранить атрибуты модели
     */
    public function save()
    {
        $path = sys_get_temp_dir() . '/' . crc32(static::class) . '.model2';
        file_put_contents($path, serialize($this->getAttributes()));
        chmod($path, 0777);
    }
}


