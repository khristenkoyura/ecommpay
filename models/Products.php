<?php


namespace models;

/**
 * Список товаров
 * Class Products
 * @package models
 */

class Products
{
    use ActiveStateModelTrait;

    protected $products = [];

    /**
     * Добавляем товар
     * @param $name
     * @param $price
     * @param $quantity
     */
    public function addProduct($name, $price, $quantity)
    {
        $this->products[] = [
            'name' => $name,
            'price' => $price,
            'quantity' => $quantity,
        ];
    }

    /**
     * Список товаров
     * @return array
     */
    public function getProducts()
    {
        return array_map(function($key, $value) {
            $value['id'] = $key;
            return $value;
        }, array_keys($this->products), $this->products);
    }

    /**
     * Покупка товара
     * @param $productId
     * @param AutoPurse $purse
     */
    public function buyAutoPurse($productId , AutoPurse $purse)
    {
        $product = & $this->products[$productId];
        $product['quantity']--;
        if ($product['quantity'] < 0) {
            throw new \InvalidArgumentException("Товар {$product['name']} закончился");
        }

        if ($product['price'] > $purse->getUserSum()) {
            throw new \InvalidArgumentException("Недостаточно средств");
        }


        $purse->buyUserSum($product['price']);
    }

    /**
     * Список товаров
     * @return array
     */
    public function getProductIds()
    {
        return array_keys($this->products);
    }
}