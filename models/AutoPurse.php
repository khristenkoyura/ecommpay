<?php
namespace  models;

/**
 * Кошелёк автомата по прадажи еды
 * Class AutoPurse
 * @package models
 */

class AutoPurse extends PurseAbstract
{
    /**
     * Сумма внесёная пользователем
     * @var integer
     */
    protected $userSum = 0;

    /**
     * @inheritdoc
     */
    public function plusCoin($coin)
    {
        parent::plusCoin($coin);
        $this->userSum+= $coin;
    }

    /**
     * @inheritdoc
     */
    public function minusCoin($coin)
    {
        parent::minusCoin($coin);
        $this->userSum-= $coin;
    }

    /**
     * Покупка на сумму
     * @param $sum
     */
    public function buyUserSum($sum)
    {
        $this->userSum-=$sum;
    }

    /**
     * Сумма пользователя
     * @return int
     */
    public function getUserSum()
    {
        return $this->userSum;
    }

    /**
     * Возвращаем деньги
     * @param PurseInterface $purse
     */
    public function resetUserSum(PurseInterface $purse)
    {
        $coins = array_reverse(self::COINS);
        foreach($coins as $coin) {
            $count = min(floor($this->userSum / $coin), $this->coins[$coin] ?? 0);
            for($i=0; $i<$count; $i++) {
                $this->shiftCoin($purse, $coin);
            }

            if ($this->userSum === 0) {
                break;
            }
        }

        if ($this->userSum > 0) {
            throw new \InvalidArgumentException('Мы не можем вам вернуть всю сумму, т.к. в автомате нет монет');
        }
    }

}
