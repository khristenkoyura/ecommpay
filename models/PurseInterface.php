<?php

namespace  models;

/**
 * Интерфейс кошелька
 * Interface PurseInterface
 */

interface PurseInterface
{
    /**
     * Список доступных монет
     */
    const COINS = [1, 2, 5, 10];

    /**
     * Забрать монету
     * @param $coin integer
     * @return void
     */
    public function minusCoin($coin);

    /**
     * Добавить монету
     * @param $coin integer
     * @return void
     */
    public function plusCoin($coin);

    /**
     * Возратить все монеты с их количеством
     * @return mixed
     */
    public function getCoins();

    /**
     * Сумма денег
     * @return integer
     */
    public function sum();


    /**
     * Переложить монету
     * @param PurseInterface $purse
     * @param $coin
     * @return mixed
     */
    public function shiftCoin(PurseInterface $purse, $coin);
}