<?php
namespace  models;


abstract class PurseAbstract implements PurseInterface
{
    use ActiveStateModelTrait;
    /**
     * Список монет с их количеством
     * @var array
     */
    protected $coins = [];

    /**
     * @inheritdoc
     */
    public function minusCoin($coin)
    {
        if (empty($this->coins[$coin])) {
            throw new \InvalidArgumentException("Нет в наличии монеты $coin");
        }
        $this->coins[$coin]--;
    }

    /**
     * @inheritdoc
     */
    public function plusCoin($coin)
    {
        if (!isset($this->coins[$coin])) {
            $this->coins[$coin] = 0;
        }

        $this->coins[$coin]++;
    }

    /**
     * @inheritdoc
     */
    public function getCoins()
    {
        $coins = [];
        foreach(static::COINS as $coin) {
            $coins[] = [
                'coin' => $coin,
                'quantity' => $this->coins[$coin] ?? 0,
            ];
        }

        return $coins;
    }

    /**
     * @inheritdoc
     */
    public function sum()
    {
        $sum = 0;
        foreach ($this->coins as $coin => $quantity) {
            $sum+=$coin * $quantity;
        }

        return $sum;
    }

    /**
     * @inheritdoc
     */
    public function shiftCoin(PurseInterface $purse, $coin)
    {
        $this->minusCoin($coin);
        $purse->plusCoin($coin);
    }

}
