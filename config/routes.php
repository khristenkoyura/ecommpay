<?php

return [
    '/coin-add' => '\\actions\\CoinAdd',
    '/coin-reset' => '\\actions\\CoinReset',
    '/product-buy' => '\\actions\\ProductBuy',
    '/state' => '\\actions\\State',
    '/init' => '\\actions\\Init',
];