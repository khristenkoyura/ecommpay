<?php

require __DIR__ . '/../vendor/autoload.php';

$routes= require __DIR__ . '/../config/routes.php';

$router = new \base\Router($routes);
$router->run();
